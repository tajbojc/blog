from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from PIL import Image


# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=100)
    image = models.ImageField(null=True, blank=True, upload_to='post_images')
    content = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width >300:
            output_size = (400, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)
    


    def __str__(self):
        return self.title
    
    
    
    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk':self.pk})
    

    
    
       


class Comment(models.Model):
    post = models.ForeignKey(Post, related_name="comments",  on_delete=models.CASCADE)       
    name = models.CharField(max_length=255)
    body = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    
    
    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk':self.pk})
    
    