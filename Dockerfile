FROM ubuntu

#CMD ["apt", "update"]
#CMD ["apt", "install", "-y", "python3"]

RUN apt-get update && apt-get install -y python3 pip

RUN pip install django

COPY . /tmp/blog/  

WORKDIR /tmp/blog/

RUN pip install django-crispy-forms

RUN pip install crispy-bootstrap4

RUN pip install Pillow

RUN pip install tzdata

RUN pip install whitenoise

RUN python3 manage.py collectstatic --no-input



#EXPOSE 8000






CMD [ "python3", "manage.py", "runserver", "0.0.0.0:8000"]

# odakle i gdje tj. odakle na sta /POM usr/Qxf2

